# README #

Simple sample application demoing offline mobile data storage and syncing to CouchDB with PouchDB.

Created for the 2015 [dev.Objective()](http://devobjective.com) conference to accompany a presentation entitled, ["Naked and Afraid: Mobile Offline Access to Emergency Data."](http://www.devobjective.com/sessions/naked-and-afraid-mobile-offline-access-to-emergency-data/)

## Technologies Used In This Project ##

* The usual smattering of HTML, CSS, JavaScript, etc.
* [Apache Cordova](http://cordova.apache.org/)
* [Apache CouchDB](http://couchdb.apache.org/)
* [PouchDB](http://pouchdb.com/)
* [Vagrant](http://vagrantup.com/)

## Running the Project ##

### The Server Bits ###

1. Install Vagrant if you don't already have it
2. Clone this repo
3. `vagrant up`

This will fire up a Vagrant box (Ubuntu server 14.04) containing everything you need to run the project from the Vagrant VM.

### The Client Bits ###

1. Install [node.js](https://nodejs.org/)
2. Install Cordova following [all the steps here](http://cordova.apache.org/docs/en/5.0.0/guide_cli_index.md.html#The%20Command-Line%20Interface)