#!/usr/bin/env bash

echo "Running apt-get update ..."
apt-get update

echo "Installing required packages ..."
apt-get install -y build-essential nodejs npm python3-pip python3-dev python-psycopg2 software-properties-common libssl-dev libjpeg8-dev libjpeg62 libtiff4-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev libaio1 g++ git wget curl vim zip unzip libpq-dev postgresql postgresql-contrib openssl

echo "Installing Cordova CLI ..."
npm install -g cordova

echo "Adding PPA for CouchDB and installing ..."
add-apt-repository -y ppa:couchdb/stable
apt-get update -y
apt-get install -y couchdb

echo "Changing bind address for CouchDB and restarting ..."
sed -i -e 's/;bind_address = 127.0.0.1/bind_address = 0.0.0.0/g' /etc/couchdb/local.ini
restart couchdb

echo "Provisioning complete."
