var db = new PouchDB('zompocalypse', {auto_compaction: true});

function saveItem() {
	var item = {};
	
	item.itemType = $('#itemType').val();
	item.itemName = $('#itemName').val();
	item.itemValue = $('#itemValue').val();
	item.notes = $('#notes').val();
	
	console.log(item);
	
	// generate an id for new items or grab existing id
	idval = $('#_id').val();
	if (idval == '') {
		item._id = new Date().getTime() + '';
	} else {
		item._id = idval;
	}
	
	db.put(item, function(err, res) {
		if (err) {
			console.log(err);
		} else if (res && res.ok) {
			console.log(res);
		}
	});
}

var map = function(doc) {
	var itemType = location.search.split('type=')[1];
	if (doc.itemType == itemType) {
		emit(doc.itemName, null);
	}
}

function getItems() {
	db.query(map, {include_docs: true}).then(function(res) {
		if (res.rows.length > 0) {
			for (var i = 0; i < res.rows.length; i++) {
				$('#itemRows').append('<tr><td><a href="javascript:void(0);" onclick="$(\'#' + res.rows[i].doc._id + '_modal\').modal(\'show\');">' + 
					res.rows[i].doc.itemName + '</a></td></tr>');
				
				var modalHTML = '<div class="modal fade" id="' + res.rows[i].doc._id + '_modal"><div class="modal-dialog"><div class="modal-content"><div class="modal-header">' + 
						'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + 
						'<h4 class="modal-title">' + res.rows[i].doc.itemName + '</h4></div>' + 
						'<div class="modal-body"><p>' + res.rows[i].doc.itemValue + '</p><p class="text-muted">' + res.rows[i].doc.notes + '</p></div>' + 
						'</div></div></div>';
				$('#modals').append(modalHTML);
			}
		} else {
			$('#itemRows').append('<tr><td>No items!</td></tr>');
		}
	}).catch(function(err) {
		console.log(err);
	});
}

function getItem(id) {
}

function destroyDB() {
	var db = new PouchDB('zompocalypse');
	db.destroy().then(function() {
		alert('Database destroyed!');
	}).catch(function(error) {
		console.log(error);
	});
}